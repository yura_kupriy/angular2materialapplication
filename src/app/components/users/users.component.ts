import {Component, OnInit} from '@angular/core';
import {MdSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {Links} from '../../app.routes';
import {User, UserKeys} from '../../models/user';
import {Order} from '../../enums/order';
import {UserService} from '../../services/user/user.service';
import Optional from '../../optional';
import * as _ from 'lodash';
const {ID, NAME, SURNAME} = UserKeys;

const userDeletedSnackBarSetup = [
  'User has been deleted successfully', '', {duration: 4000}
];
const userNotDeletedSnackBarSetup = [
  'User was not deleted', '', {duration: 3000}
];

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: User[] = [];

  constructor(private router: Router, private snackBar: MdSnackBar, private userService: UserService) {
    Object.assign(this, {
      sort  : {
        keys  : [NAME],
        orders: [Order.ASC]
      },
      search: {
        keys: [NAME, SURNAME]
      }
    });
  }

  ngOnInit() {
    this.userService.getAll().subscribe(this.setUsers);
  }

  editUser(user: User) {
    Optional
      .ofNullable(user)
      .map(_.property(ID))
      .ifPresent(this.redirectByUserId);
  }

  setUsers = (users) => this.users = users;

  deleteUser(user: User) {
    Optional
      .ofNullable(user)
      .map(_.property(ID))
      .ifPresent(this.startDeleteUserProcess);
  }

  startDeleteUserProcess = (id) => {
    this.userService.deleteOne(id).subscribe(() =>
        this.setUsers(_(this.users).reject([ID, id])),
        this.removeUserFailure,
        this.removeUserComplete
      );
  }

  redirectByUserId = (id) => this.router.navigate([Links.USERS, id]);

  removeUserComplete = () => this.snackBar.open(...userDeletedSnackBarSetup);

  removeUserFailure = () => this.snackBar.open(...userNotDeletedSnackBarSetup);

}

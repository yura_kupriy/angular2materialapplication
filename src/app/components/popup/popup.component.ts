import { Component } from '@angular/core';
import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent {
  //@Input() @TODO create input question;question: string;
  constructor(public dialogRef: MdDialogRef<PopupComponent>) {}

}

import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../models/user';
import { Gender } from '../../enums/gender';
import { ValidationService } from '../../services/validation/validation.service';
import { USER_FORM_VALIDATION_MESSAGES } from './user-form.messages';

let USER_FORM_SETUP = {
	'name'   : ['', [ Validators.required, Validators.minLength(4), Validators.maxLength(15)]],
	'surname': ['', [ Validators.required, Validators.minLength(3), Validators.maxLength(15)]],
	'phone'  : ['', [ Validators.required, ValidationService.phoneValidator, Validators.minLength(10), Validators.maxLength(15)]],
	'email'  : ['', [ Validators.required, ValidationService.emailValidator]],
	'gender' : ['', [ Validators.required, ValidationService.genderValidator]]
};

let USER_FORM_ERRORS_HOLDER = {
	'name'    : '',
	'surname' : '',
	'email'   : '',
	'phone'   : '',
	'gender'  : ''
};

@Component({
	selector: 'app-user-form',
	templateUrl: './user-form.component.html',
	styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
	user: User;
	userForm: FormGroup;
	formErrors: Object = USER_FORM_ERRORS_HOLDER;
	validationMesagess = USER_FORM_VALIDATION_MESSAGES;

	@Input('user')
	set updateUser(newUser) {
		this.user = newUser;
		if (!!this.user)
			this.fillForm(this.user);
	}
	@Output() userSubmitted = new EventEmitter();

	constructor(private formBuilder: FormBuilder) {
		Object.assign(this, {Gender});
	}

	ngOnInit() {
		this.setForm();
		this.validateForm();
		this.subscribeOnFormChanges();
	}

	setForm(): void {
		this.userForm = this.formBuilder.group(USER_FORM_SETUP);
	}

	subscribeOnFormChanges(): void {
		this.userForm
		.valueChanges
		.subscribe(this.validateForm);
	}
  
  validateForm = () => {
    const form = this.userForm;
    
    this.formErrors = Object
      .keys(USER_FORM_ERRORS_HOLDER)
      .map((field) => ({
        field,
        control : form.get(field),
        messages: USER_FORM_VALIDATION_MESSAGES[field]
      }))
      .reduce((errors, {field, control, messages}) => {
        if (control && control.dirty && !control.valid) {
          for (const key in control.errors) {
            errors[field] += `${messages[key]}  `;
          }
          return errors;
        }
      }, USER_FORM_ERRORS_HOLDER);
  };

	fillForm({name, surname, email, phone, gender}): void {
		this.userForm.setValue({ name, surname, email, phone, gender }, { onlySelf: true });
	}

	onSubmit(form) {
		let user = Object.assign({}, form.value, { id: this.user && this.user.id ? this.user.id : null});
		this.userSubmitted.emit(user);
	}
}

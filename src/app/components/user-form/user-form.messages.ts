export const USER_FORM_VALIDATION_MESSAGES = {
	'name': {
		'required' : 'Name is required.',
		'minlength': 'Name must be at least 4 characters long.',
		'maxlength': 'Name cannot be more than 15 characters long.'
	},
	'email': {
		'required' : 'Email is required.',
		'invalid'  : 'Email must be correct'
	},
	'phone': {
		'required' : 'Phone is required.',
		'invalid'  : 'Phone must be correct',
		'minlength': 'Phone must be at least 10 characters long.',
		'maxlength': 'Phone cannot be more than 10 characters long.'
	},
	'surname': {
		'required' : 'Surname is required.',
		'minlength': 'Surname must be at least 4 characters long.',
		'maxlength': 'Surname cannot be more than 15 characters long.'
	},
    'gender': {
        'required' : 'Gender is required',
        'invalid'  : 'Gender must be selected'
    }
};

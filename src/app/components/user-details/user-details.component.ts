import {Component, OnInit} from '@angular/core';
import {MdSnackBar} from '@angular/material';
import {Links} from '../../app.routes';
import {User} from '../../models/user';
import {UserService} from '../../services/user/user.service';
import {Router, ActivatedRoute} from '@angular/router';
import Optional from '../../optional';
import {UserFilters} from '../../filters/user.filters';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  user: User;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    private snackBar: MdSnackBar
  ) {}

  ngOnInit() {
    this.route.params
    .map(({id}) => id)
    .filter(x => !!x && x >= 1)
    .subscribe(this.getUser.bind(this));
  }

  onFormSubmit(user: any) {
    this.checkUserIsPresent(user);
    this.saveNewUserIfPresent(user);
    this.updateExistingUserIfPresent(user);
  }

  getUser(id) {
    return this.userService.getOne(id).subscribe(this.setUser.bind(this));
  }

  setUser(user) {
    this.user = user;
  }

  checkUserIsPresent(user) {
    Optional
      .ofNullable(user)
      .orElseGet(this.userSaveFailure.bind(this));
  }

  saveNewUserIfPresent(user) {
    Optional
      .ofNullable(user)
      .filter(UserFilters.ID_NOT_EXIST)
      .ifPresent(this.saveUser.bind(this, user));
  }

  updateExistingUserIfPresent(user) {
    Optional
      .ofNullable(user)
      .filter(UserFilters.ID_EXIST)
      .ifPresent(this.updateUser.bind(this));
  }

  updateUser(user) {
    return this.userService.update(user).subscribe(
      this.userSaveSuccess.bind(this),
      this.userSaveFailure.bind(this),
      this.redirectToUsers.bind(this)
    );
  }

  saveUser(user) {
    return this.userService.save(user).subscribe(
      this.userSaveSuccess.bind(this),
      this.userSaveFailure.bind(this),
      this.redirectToUsers.bind(this)
    );
  }

  userSaveSuccess() {
    this.snackBar.open('User saved', '', {duration: 5000});
  }

  userSaveFailure() {
    this.snackBar.open('Something went wrong', '', {duration: 6000});
  }

  redirectToUsers() {
    this.router.navigate([Links.USERS]);
  }

}

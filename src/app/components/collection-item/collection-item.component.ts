import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { PopupComponent } from '../popup/popup.component';

@Component({
	selector: 'app-collection-item',
	templateUrl: './collection-item.component.html',
	styleUrls: ['./collection-item.component.scss']
})
export class CollectionItemComponent {
	@Input() item: any;
	@Input() title: any = '';
	@Input() image: any = `https://d30y9cdsu7xlg0.cloudfront.net/png/138926-200.png`;
	@Input() subtitle: any;
	@Input() isActive: boolean = false;

	@Output() onItemSelected = new EventEmitter();
	@Output() onItemDeleted = new EventEmitter();

	constructor(public dialog: MdDialog) {}

	dialogRef: MdDialogRef<PopupComponent>;

	onSelect(item) {
		this.onItemSelected.emit(item);
	}

	onDelete(item) {
		this.onItemDeleted.emit(item);
	}

	openDialog(model) : void {
		this.dialogRef = this.dialog.open(PopupComponent);
		this.dialogRef.afterClosed()
					  .subscribe(result => this.onOpened(result, model));
	}

	onOpened(isPressedYes: boolean, model: any): void {
		if (isPressedYes) {
			this.onDelete(model);
		}
		this.dialogRef = null;
	}

}

/* tslint:disable:no-unused-variable */

import {TestBed, async, inject} from '@angular/core/testing';
import {ValidationService} from './validation.service';

describe('ValidationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValidationService]
    });
  });

  it('should ...', inject([ValidationService], (service: ValidationService) => {
    expect(service).toBeTruthy();
  }));

  it('should validate correct email', () => {
    expect(ValidationService.emailValidator({value: 's.a@email.com'})).toEqual(null);
  });

  it('should validate incorrect email', () => {
    expect(ValidationService.emailValidator({value: 's.a@email'})).toEqual({invalid: true});
  });
});

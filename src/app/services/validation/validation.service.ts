import {Injectable} from '@angular/core';
import {Gender} from '../../enums/gender';

const VALID_CONTROL = null; //angular specs
const INVALID_CONTROL = {invalid: true};

const GENDERS = [Gender.MALE, Gender.FEMALE];
const PHONE_REGEXP = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
const EMAIL_REGEXP = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

@Injectable()
export class ValidationService {

  static emailValidator(control) {
    return control.value.match(EMAIL_REGEXP)
      ? VALID_CONTROL
      : INVALID_CONTROL;
  }

  static phoneValidator(control) {
    return control.value.match(PHONE_REGEXP)
      ? VALID_CONTROL
      : INVALID_CONTROL;
  }

  static genderValidator(control) {
    return GENDERS.some(gender => gender === control.value)
      ? VALID_CONTROL
      : INVALID_CONTROL;
  }

}

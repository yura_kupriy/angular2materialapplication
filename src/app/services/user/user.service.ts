import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Http} from '@angular/http';
import {User} from '../../models/user';

const USERS_API = `http://584fc4d576354b1200e88750.mockapi.io/api/v1/users`;

const mapToUser  = (response) => <User>response.json();
const mapToUsers = (response) => <User[]>response.json();
const catchError = (error: any) => Observable.throw(error.json().error);

@Injectable()
export class UserService {
  constructor(private http: Http, private url: USERS_API) {}

  getOne(id: number): Observable<User> {
    return this.http
      .get(`${this.url}/${id}`)
      .map(mapToUser)
      .catch(catchError);
  }

  getAll(): Observable<User[]> {
    return this.http
      .get(this.url)
      .map(mapToUsers)
      .catch(catchError);
  }

  save(user: User): Observable<User> {
    return this.http
      .post(this.url, user)
      .map(mapToUser)
      .catch(catchError);
  }

  update(user: User): Observable<User> {
    return this.http
      .put(`${this.url}/${user.id}`, user)
      .map(mapToUser)
      .catch(catchError);
  }

  deleteOne(id: number): Observable<User> {
    return this.http
      .delete(`${this.url}/${id}`)
      .map(mapToUser)
      .catch(catchError);
  }
}

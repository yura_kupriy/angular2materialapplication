/* tslint:disable:no-unused-variable */
import {TestBed, async, inject} from '@angular/core/testing';
import {
  HttpModule,
  Http,
  Response,
  ResponseOptions,
  XHRBackend
} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import {UserService} from './user.service';

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [{provide: MockBackend, useClass: MockBackend}, UserService]
    });
  });

  it('should ...', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));

  it('should return an Observable<Array<Users>>',
    inject([UserService, MockBackend], (userService, mockBackend) => {

      const mockResponse = {
        data: [
          {id: 0, name: 'User 0'},
          {id: 1, name: 'User 1'},
          {id: 2, name: 'User 2'},
          {id: 3, name: 'User 3'},
        ]
      };

      mockBackend.connections.subscribe((connection) => {
        connection.mockRespond(new Response(new ResponseOptions({
          body: JSON.stringify(mockResponse)
        })));
      });

      userService.getUsers().subscribe((users) => {
        expect(users.length).toBe(4);
        expect(users[0].name).toEqual('User 0');
        expect(users[1].name).toEqual('User 1');
        expect(users[2].name).toEqual('User 2');
        expect(users[3].name).toEqual('User 3');
      });

    }));
});

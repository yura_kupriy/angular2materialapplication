export interface IUser {
	id: number;
	name: string;
	surname: string;
	email: string;
	gender: string;
	phone: string;
	avatar?: string;
}
import {Pipe, PipeTransform} from '@angular/core';
import {Order} from '../../enums/order';
import * as _ from 'lodash';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(items: Array<any>, orders: Array<Order> = [], keys: Array<string> = []): any {
    return _.orderBy(items, keys, orders);
  }

}

/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { SortPipe } from './sort.pipe';
import {Order} from "../../enums/order";

describe('SortPipe', () => {
  it('Create an instance', () => {
    let pipe = new SortPipe();
    expect(pipe).toBeTruthy();
  });

  it('Should return an array of strings ordered by ASC', () => {
    let pipe = new SortPipe();
    let order = Order.ASC;
    let initialValues = ['b', 'd', 'c', 'a'];
    let expectedValues = ['a', 'b', 'c', 'd'];

    expect(pipe.transform(initialValues, [order], [])).toEqual(expectedValues);
  });

  it('Should return an array of strings ordered by DESC', () => {
    let pipe = new SortPipe();
    let order = Order.DESC;
    let initialValues = ['b', 'd', 'c', 'a'];
    let expectedValues = ['d', 'c', 'b', 'a'];

    expect(pipe.transform(initialValues, [order], [])).toEqual(expectedValues);
  });

  it('Should return a collection ordered by keys in ASC order', () => {
    let pipe = new SortPipe();
    let key = 'name';
    let order = Order.ASC;
    let initialValues = [{[key]: 'd'}, {[key]: 'a'}, {[key]: 'c'}, {[key]: 'b'}];
    let expectedValues = [{[key]: 'a'}, {[key]: 'b'}, {[key]: 'c'}, {[key]: 'd'}];

    expect(pipe.transform(initialValues, [order], [key])).toEqual(expectedValues);
  });

  it('Should return a collection ordered by keys in DESC order', () => {
    let pipe = new SortPipe();
    let key = 'name';
    let order = Order.DESC;
    let initialValues = [{[key]: 'd'}, {[key]: 'a'}, {[key]: 'c'}, {[key]: 'b'}];
    let expectedValues = [{[key]: 'd'}, {[key]: 'c'}, {[key]: 'b'}, {[key]: 'a'}];

    expect(pipe.transform(initialValues, [order], [key])).toEqual(expectedValues);
  });
});

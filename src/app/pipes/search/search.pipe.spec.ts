/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { SearchPipe } from './search.pipe';

describe('SearchPipe', () => {
  it('Create an instance', () => {
    let pipe = new SearchPipe();
    expect(pipe).toBeTruthy();
  });

  it('Should return values that match the term', () => {
    let pipe = new SearchPipe();
    let key = 'name';
    let term = 'ab';
    let initialValues = [{[key]: 'abc'}, {[key]: 'def'}];
    let expectedValues = [{[key]: 'abc'}];

    expect(pipe.transform(initialValues, [key], term)).toEqual(expectedValues);
  });

  it('Should return values if the term is empty', () => {
    let pipe = new SearchPipe();
    let key = 'name';
    let term = '';
    let initialValues = [{[key]: 'abc'}, {[key]: 'def'}];

    expect(pipe.transform(initialValues, [key], term)).toEqual(initialValues);
  });

  it('Should return the values if term is undefined', () => {
    let pipe = new SearchPipe();
    let key = 'name';
    let term = undefined;
    let initialValues = [{[key]: 'abc'}, {[key]: 'def'}];

    expect(pipe.transform(initialValues, [key], term)).toEqual(initialValues);
  });

  it('Should return the values if term is null', () => {
    let pipe = new SearchPipe();
    let key = 'name';
    let term = null;
    let initialValues = [{[key]: 'abc'}, {[key]: 'def'}];

    expect(pipe.transform(initialValues, [key], term)).toEqual(initialValues);
  });

  it('Should return an empty array if values are empty', () => {
    let pipe = new SearchPipe();
    let key = 'name';
    let term = 'ab';
    let initialValues = [];

    expect(pipe.transform(initialValues, [key], term)).toEqual([]);
  });
});

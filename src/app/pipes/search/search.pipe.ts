import {Pipe, PipeTransform} from '@angular/core';
import * as _ from 'lodash';
import {EMPTY_STRING, REGEXP_EMPTY_STRING} from "../../constants/constants";

@Pipe({
  name: 'search'
})

export class SearchPipe implements PipeTransform {

  transform(items: Array<any>, keys: Array<string> = [], term: string = ''): any {
    return term ? items.filter(it =>
      _(it)
        .pick(keys)
        .values()
        .join(EMPTY_STRING)
        .match(new RegExp(String(term).replace(REGEXP_EMPTY_STRING, EMPTY_STRING), 'gi'))
    ) : items;
  }

}

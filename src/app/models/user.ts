import {IUser} from '../interfaces/user';

export enum UserKeys {
  ID = 'id' as any,
  NAME = 'name' as any,
  SURNAME = 'surname' as any,
  EMAIL = 'email' as any,
  GENDER = 'gender' as any,
  AVATAR = 'avatar' as  any
}

export class User implements IUser {
  constructor(public id: number,
              public name: string,
              public surname: string,
              public email: string,
              public gender: string,
              public phone: string,
              public avatar: string) {
  }
}

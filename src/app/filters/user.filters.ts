export const USER_FILTER = {
  ID_EXIST: 'ID_EXIST',
  ID_NOT_EXIST: 'ID_NOT_EXIST'
};

const {ID_EXIST, ID_NOT_EXIST} = USER_FILTER;

export const UserFilters = {
  ID_EXIST: user => !!user['id'],
  ID_NOT_EXIST: user => user['id'] == null
};

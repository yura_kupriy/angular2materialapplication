import {BrowserModule} from '@angular/platform-browser';
import {MaterialModule} from '@angular/material';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {ROUTES} from './app.routes';

/* Pipes */
import {SearchPipe} from './pipes/search/search.pipe';
import {SortPipe} from './pipes/sort/sort.pipe';

/* Services */
import {UserService} from './services/user/user.service';
import {ValidationService} from './services/validation/validation.service';

/*Components*/
import {AppComponent} from './app.component';
import {UsersComponent} from './components/users/users.component';
import {UserFormComponent} from './components/user-form/user-form.component';
import {CollectionComponent} from './components/collection/collection.component';
import {CollectionItemComponent} from './components/collection-item/collection-item.component';
import {SearchBoxComponent} from './components/search-box/search-box.component';
import {UserDetailsComponent} from './components/user-details/user-details.component';
import {PopupComponent} from './components/popup/popup.component';

const IMPORTS = [
  BrowserModule,
  FormsModule,
  ReactiveFormsModule,
  HttpModule,
  RouterModule.forRoot(ROUTES, {useHash: true}),
  MaterialModule.forRoot()
];
const PROVIDERS = [UserService, ValidationService];

@NgModule({
  declarations: [
    SearchPipe,
    AppComponent,
    UsersComponent,
    UserFormComponent,
    CollectionComponent,
    CollectionItemComponent,
    SearchBoxComponent,
    UserDetailsComponent,
    PopupComponent,
    SortPipe
  ],
  imports: IMPORTS,
  providers: PROVIDERS,
  entryComponents: [PopupComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
